{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Control.Monad.IO.Class (liftIO)
import qualified Data.ByteString.Char8 as BS
import Data.Function (fix)
import qualified Data.Text as T (Text, pack, unpack)
import qualified Data.Text.Lazy as LT (fromStrict, pack)
import qualified Data.Text.Encoding as T (encodeUtf8)
import Ldap.Client as Ldap
import qualified Ldap.Client.Bind as Ldap
import System.Environment (getEnv)
import Web.Scotty

data LdapConf = LdapConf
  { host     :: String
  , port     :: PortNumber
  , dn       :: Dn
  , password :: Password
  , base     :: Dn
  } deriving (Show, Eq)

getConf :: IO (LdapConf)
getConf = do
  ldapHost <- getEnv "LDAP_HOST"
  ldapPort <- getEnv "LDAP_PORT" >>= (return . read)
  ldapDn <- getEnv "LDAP_DN" >>= (return . T.pack)
  ldapPassword <- getEnv "LDAP_PASSWORD" >>= (return . BS.pack)
  ldapBase <- getEnv "LDAP_BASE" >>= (return . T.pack)
  return $ LdapConf ldapHost ldapPort (Ldap.Dn ldapDn) (Ldap.Password ldapPassword) (Ldap.Dn ldapBase)

main :: IO ()
main = do
  conf <- getConf
  port <- getEnv "PORT" >>= (return . read)
  scotty port $
    get "/users/:user" $ do
      user <- param "user" >>= (return . T.pack)
      res  <- liftIO $ login conf user
      case res of
        Left  e -> text (LT.pack $ show e)
        Right v -> text (LT.fromStrict v)

login :: LdapConf -> T.Text -> IO (Either LdapError T.Text)
login conf user =
  Ldap.with (Plain (host conf)) (port conf) $ \l -> do
    Ldap.bind l (dn conf) (password conf)
    us  <- Ldap.search l (base conf)
                         (typesOnly False)
                         (And [ Attr "objectClass" := "account"
                              , Attr "uid" := T.encodeUtf8 user
                              ])
                         []
    return (T.pack $ show us)
